import MarkdownIt from "markdown-it";

import { Previewer, Handler, registerHandlers } from "pagedjs";
import "./paged.interface.css";
import "./style.css";

/* Initialisation de variables */
const md = new MarkdownIt();
const previewer = new Previewer();
let nosPagesImage;

/* Outils */

async function loadPadToHtml() {
  const padDiv = document.getElementById("pad-content");
  const padUrl = padDiv.getAttribute("data-md");
  const docDiv = document.getElementById("document");
  await fetch(padUrl)
    .then((res) => res.text())
    .then((data) => {
      let newDiv = document.createElement("div");
      newDiv.innerHTML = md.render(data);
      docDiv.appendChild(newDiv);
    });
}

async function fetchArray() {
  let result;
  /*récupération du tableau des images bien rangées */
  await fetch("/imgs-array")
    .then((res) => res.json())
    .then((data) => (result = data));
  return result;
}

/* Classes */

class PagesImage {
  constructor(_imgArray) {
    this.imgArray = _imgArray;
    this.imgsToDispatch = _imgArray.length - 1;
    this.placesDispo = 4;
    this.counterImgPass = 0;
    this.pages = [];
  }
  get haveImgsToDispatch() {
    console.log(
      "🚀 ~ file: main.js ~ line 112 ~ PagesImage ~ gethaveImgsToDispatch ~ this.imgToDispatch",
      this.imgsToDispatch
    );
    return this.imgsToDispatch > 1;
  }
  pagesEntree(i) {
    return this.pages[i];
  }
  organizeDispatch() {
    let tempPlacesDispo = this.placesDispo;
    let htmlString = "";
    let counter = 0;
    htmlString += `<div class="imgsWrap">`;
    console.log(this.imgArray);
    while (tempPlacesDispo > 0) {
      console.log(this.counterImgPass);
      console.log(counter);
      console.log(this.imgArray[this.counterImgPass + counter]);
      if (this.imgArray[this.counterImgPass + counter]) {
        if (
          this.imgArray[this.counterImgPass + counter].dimensions.height >
            this.imgArray[this.counterImgPass + counter].dimensions.width ||
          this.imgArray[this.counterImgPass + counter].dimensions
            .orientation === 6
        ) {
          // Créer une class r-2
          tempPlacesDispo -= 2;
          if (tempPlacesDispo >= 0) {
            htmlString += `<img loading="lazy"  class="img-r-2" src="./imgs/${
              this.imgArray[this.counterImgPass + counter].name
            }" />`;
            counter++;
          } else {
            break;
          }
        } else {
          // Créer une class r-1
          tempPlacesDispo -= 1;
          if (tempPlacesDispo >= 0) {
            htmlString += `<img loading="lazy" class="img-r-1" src="./imgs/${
              this.imgArray[this.counterImgPass + counter].name
            }" />`;
            counter++;
          } else {
            break;
          }
        }
      } else {
        break;
      }
    }

    htmlString += `</div>`;
    this.pages.push(htmlString);
    this.imgsToDispatch -= counter;
    this.counterImgPass += counter;
  }
}

class MyHandler extends Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforePageLayout(page) {
    console.log(nosPagesImage.haveImgsToDispatch);
    if (page.position > -1 && nosPagesImage.haveImgsToDispatch) {
      this.chunker.addPage(false);
      nosPagesImage.organizeDispatch();
      this.chunker.addPage(false);
      nosPagesImage.organizeDispatch();
    }
  }
  afterRendered(pages) {
    let counter = 0;
    pages.map((page) => {
      if (page.area && !page.area.innerHTML) {
        page.area.innerHTML = nosPagesImage.pagesEntree(counter);
        console.log(
          "🚀 ~ file: main.js ~ line 126 ~ MyHandler ~ pages.map ~ page",
          page
        );
        //page.element.classList.contains("pagedjs_left_page")
        console.log(
          "🚀 ~ file: main.js ~ line 128 ~ MyHandler ~ pages.map ~ page.element.classList.contains(pagedjs_left_page)",
          page.element.classList.contains("pagedjs_left_page")
        );

        if (page.element.classList.contains("pagedjs_left_page")) {
          //  page.element.getElementsByClassName("pagedjs_margin-bottom-right-corner-holder")[0].innerHTML("Super !")
          console.log(
            "🚀 ~ file: main.js ~ line 139 ~ MyHandler ~ pages.map ~  page.element.getElementsByClassName(pagedjs_margin-bottom-right-corner-holder)",
            );
           page.element.getElementsByClassName(
              "pagedjs_margin-bottom-left-corner-holder"
            )[0].innerHTML  = 'couoccucouuouocuouco'
        }

        //page.element.classList.contains("pagedjs_right_page")
        console.log(
          "🚀 ~ file: main.js ~ line 130 ~ MyHandler ~ pages.map ~ page.element.classList.contains(pagedjs_right_page)",
          page.element.classList.contains("pagedjs_right_page")
        );

        // TODO: page.location.innerHTML = page.position
        counter++;
      }
    });
  }
}

registerHandlers(MyHandler);

var reload = setTimeout("location.reload(true);", 500000);

function scrollBottom() {
  reload;
  window.scrollTo(0, document.body.scrollHeight);
  console.log("coucou");
}

/* Event pour le démarrage */
document.addEventListener("DOMContentLoaded", async function (event) {
  console.log("Hello App");
  await loadPadToHtml();
  let imgArray = await fetchArray();
  nosPagesImage = new PagesImage(imgArray);
  await previewer.preview();
  scrollBottom();
});
