import path from "path";
import fs from "fs";
import fse from "fs-extra";
import express from "express";
import imageSize from "image-size";

const app = express(),
  DIST_DIR = `${__dirname}/public`,
  HTML_FILE = path.join(DIST_DIR, "index.html");

/* Ajout du dossier public */
app.use(express.static(DIST_DIR));

/* Routage */
/* Route base */
app.get("/", (req, res) => {
  res.sendFile(HTML_FILE);
});

/* Route pour envoyer un objet représentant les images et copier le dossier image */
app.get("/imgs-array", async (req, res) => {
  let response = await getSortedFiles("./imgs_from_smb");
  res.send(response);

  const srcDir = `./imgs_from_smb`;
  const destDir = `./dist/public/imgs`;
  fse.copySync(srcDir, destDir, { overwrite: true }, function (err) {
    if (err) {
      console.error(err);
    } else {
      console.log("success!");
    }
  });
});

/* Démarrage du serveur */
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Les serveur a démarrer sur le port ${PORT}`);
});

/* Outils */

/**
 * Retourne un tableau des fichiers rangé par ordre de modification
 * @param {String} dir
 * @returns {[{name: String, dimensions: {type: String, width: Number, height: Number}}]}
 */
const getSortedFiles = async (dir) => {
  const files = await fs.promises.readdir(dir);

  return files
    .map((fileName) => ({
      name: fileName,
      time: fs.statSync(`${dir}/${fileName}`).mtime.getTime(),
    }))
    .filter((item) => !/(^|\/)\.[^\/\.]/g.test(item.name))
    .sort((a, b) => a.time - b.time)
    .map((file) => ({
      name: file.name,
      dimensions: imageSize(`./imgs_from_smb/${file.name}`),
    }));
};
